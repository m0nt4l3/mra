package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {



	private int xMax ;
	private int yMax ;

	private int xPositionRover ;
	private int yPositionRover ;

	private String nullCommandString = "0,0";

	private String direction;

	private String roverPositionInplanet;



	private List<String> obstacles = new ArrayList<>();



	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {

		xMax = planetX;
		yMax = planetY;

		obstacles.addAll(planetObstacles);


	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		String checkObstacles = x+","+y;

		System.out.print(checkObstacles);

		if(obstacles.contains(checkObstacles)) {

			return true;

		}else {

			return false;
		}


	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return 
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */

	public void roverPosition(){

		xPositionRover = 0;
		yPositionRover = 0;
		direction ="N";
		roverPositionInplanet = "0,0";

	}

	public void setRoverPosition(int x, int y){

		xPositionRover = x;
		yPositionRover = y;
		roverPositionInplanet = x+","+y;

	}

	public String getRoverPosition(){

		return roverPositionInplanet;

	}
	public void setDirection(String trmpString){

		direction = trmpString;

	}

	public String getDirection(){

		return direction;

	}
	public String executeCommand(String commandString) throws MarsRoverException {




		String newPositionRover = "";

		List<String> obstaclesFind = new ArrayList<>();

		if(commandString.length()>1) {

			for(int j=0;j<commandString.length();j++) {

				System.out.print(commandString.charAt(j));
				executeCommand(String.valueOf(commandString.charAt(j)));
				
				
			}

			if(obstaclesFind.size()>0) {

				newPositionRover = getRoverPosition() +","+direction;

				for(int i=0; i<obstaclesFind.size();i++) {

					newPositionRover +=","+ obstaclesFind.get(i);

				}


			}else {

				newPositionRover = getRoverPosition()+","+direction;
			}


			return newPositionRover;



		}else {

			switch(commandString) {

			case "f" : 



				switch(direction) {

				case "N":

					if(planetContainsObstacleAt(xPositionRover,yPositionRover+1)){

						obstaclesFind.add((xPositionRover) +","+ (yPositionRover+1));

						setRoverPosition(xPositionRover,yPositionRover+1);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);

						}



					}else {


						setRoverPosition(xPositionRover,yPositionRover+1);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				case "S" : 


					if(planetContainsObstacleAt(xPositionRover,yPositionRover-1)){

						obstaclesFind.add((xPositionRover) +","+ (yPositionRover-1));

						setRoverPosition(xPositionRover,yPositionRover-1);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover,yPositionRover-1);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				case "E" : 	


					if(planetContainsObstacleAt(xPositionRover+1,yPositionRover)){

						obstaclesFind.add((xPositionRover+1) +","+ (yPositionRover));

						setRoverPosition(xPositionRover+1,yPositionRover);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover+1,yPositionRover);
						newPositionRover = getRoverPosition()+","+direction;

					}	


					break;

				case "W" : 	


					if(planetContainsObstacleAt(xPositionRover-1,yPositionRover)){

						obstaclesFind.add((xPositionRover-1) +","+ (yPositionRover));

						setRoverPosition(xPositionRover-1,yPositionRover);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover-1,yPositionRover);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				}


				break;		




			case "b" : 

				switch(direction) {

				case "N":

					if(planetContainsObstacleAt(xPositionRover,yPositionRover-1)){

						obstaclesFind.add((xPositionRover) +","+ (yPositionRover-1));

						setRoverPosition(xPositionRover,yPositionRover-1);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);

						}



					}else {


						setRoverPosition(xPositionRover,yPositionRover-1);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				case "S" : 


					if(planetContainsObstacleAt(xPositionRover,yPositionRover+1)){

						obstaclesFind.add((xPositionRover) +","+ (yPositionRover+1));

						setRoverPosition(xPositionRover,yPositionRover+1);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover,yPositionRover+1);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				case "E" : 	


					if(planetContainsObstacleAt(xPositionRover-1,yPositionRover)){

						obstaclesFind.add((xPositionRover-1) +","+ (yPositionRover));

						setRoverPosition(xPositionRover-1,yPositionRover);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover-1,yPositionRover);
						newPositionRover = getRoverPosition()+","+direction;

					}	


					break;

				case "W" : 	


					if(planetContainsObstacleAt(xPositionRover+1,yPositionRover)){

						obstaclesFind.add((xPositionRover+1) +","+ (yPositionRover));

						setRoverPosition(xPositionRover+1,yPositionRover);

						newPositionRover = getRoverPosition() +","+direction;

						for(int i=0; i<obstaclesFind.size();i++) {

							newPositionRover +=","+ obstaclesFind.get(i);
						}



					}else {


						setRoverPosition(xPositionRover+1,yPositionRover);
						newPositionRover = getRoverPosition()+","+direction;

					}


					break;

				}


				break;		

			case "l" : 

				switch(direction) {

				case "N":
					
					setDirection("W");
					newPositionRover = getRoverPosition()+","+direction;
					

					break;

				case "S" : 
					setDirection("E");
					newPositionRover = getRoverPosition()+","+direction;
					

					break;

				case "E" : 	
					setDirection("N");
					newPositionRover = getRoverPosition()+","+direction;


					break;

				case "W" : 	

					setDirection("S");
					newPositionRover = getRoverPosition()+","+direction;


					break;

				}

			case "r" :

				switch(direction) {

				case "N":
					setDirection("E");
					newPositionRover = getRoverPosition()+","+direction;


					break;

				case "S" : 
					setDirection("W");
					newPositionRover = getRoverPosition()+","+direction;


					break;

				case "E" : 	
					setDirection("N");
					newPositionRover = getRoverPosition()+","+direction;



					break;

				case "W" : 	

					setDirection("S");
					newPositionRover = getRoverPosition()+","+direction;


					break;

				}





				break;		

			default : newPositionRover = xPositionRover+","+yPositionRover+","+direction;

			}


			return newPositionRover;	



		}


	}
}
