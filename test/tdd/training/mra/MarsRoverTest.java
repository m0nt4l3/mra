package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {


	
	@Test
	public void testMarsRover() throws MarsRoverException {
																						
		int posX = 10;
		int posY = 10 ;
		
		List<String> obstacles = new ArrayList<>();
		 
		obstacles.add("4,7");
		
		
		MarsRover marsRover = new MarsRover(posX,posY,obstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(4,7));
		
	}

	@Test
	public void testMarsRoverCommandStringF() throws MarsRoverException {

		int posX = 10;
		int posY = 10;
		
		List<String> obstacles = new ArrayList<>();
		 
		obstacles.add("4,7");
		obstacles.add("0,1");
		obstacles.add("2,2");
		
		
		MarsRover marsRover = new MarsRover(posX,posY,obstacles);
		
		marsRover.roverPosition();
		
		marsRover.setDirection("N");
		
		assertEquals("0,1,N,0,1",marsRover.executeCommand("f"));
		
	}
		
	
	@Test
	public void testMarsRoverCommandStringEmpty() throws MarsRoverException {

		int posX = 10;
		int posY = 10;
		
		List<String> obstacles = new ArrayList<>();
		 
		obstacles.add("4,7");
		obstacles.add("0,1");
		obstacles.add("2,2");
		
		
		MarsRover marsRover = new MarsRover(posX,posY,obstacles);
		
		marsRover.roverPosition();
		
		
		
		assertEquals("0,0,N", marsRover.executeCommand(""));
		
	}
	
	@Test
	public void testMarsRoverCommandStringB() throws MarsRoverException {

		int posX = 10;
		int posY = 10;
		
		List<String> obstacles = new ArrayList<>();
		 
		obstacles.add("4,7");
		obstacles.add("0,1");
		obstacles.add("2,1");
		
		
		MarsRover marsRover = new MarsRover(posX,posY,obstacles);
		
		marsRover.roverPosition();
		
		marsRover.setRoverPosition(5, 8);
		
		marsRover.setDirection("E");
		
		assertEquals("4,8,E", marsRover.executeCommand("b"));
		
	}
	
	@Test
	public void testMarsRoverCommandStringR() throws MarsRoverException {

		int posX = 10;
		int posY = 10;
		
		List<String> obstacles = new ArrayList<>();
		 
		obstacles.add("4,7");
		obstacles.add("0,1");
		obstacles.add("2,1");
		
		
		MarsRover marsRover = new MarsRover(posX,posY,obstacles);
		
		marsRover.roverPosition();
		
		marsRover.setRoverPosition(2, 2);
		
		marsRover.setDirection("N");
		
		assertEquals("2,2,E", marsRover.executeCommand("r"));
		
	}

@Test
public void testMarsRoverCommandStringMore() throws MarsRoverException {

	int posX = 10;
	int posY = 10;
	
	List<String> obstacles = new ArrayList<>();
	 
	obstacles.add("4,7");
	obstacles.add("0,1");
	obstacles.add("2,1");
	
	
	MarsRover marsRover = new MarsRover(posX,posY,obstacles);
	
	marsRover.roverPosition();

	assertEquals("2,2,E", marsRover.executeCommand("ffrff"));
	
}
}
